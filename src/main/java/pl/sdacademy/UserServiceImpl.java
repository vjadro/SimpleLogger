package pl.sdacademy;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class UserServiceImpl implements UserService {

    private List<User> users;
    protected static final Logger log = Logger.getLogger(UserServiceImpl.class.getName());

    public UserServiceImpl() {
        users = new ArrayList<>();
    }

    @Override
    public boolean registerUser(User user) {
        for(User u : users) {
            if ((u.getLogin() == user.getLogin()) || (u.getEmail() == user.getEmail())) {
                log.warn("Registration error: User login or e-mail already in DB.");
                return false;
            }
            if (!isAdult(u))
                return false;
        }

        users.add(user);
        log.info("User registered: " + user.getLogin());
        return true;
    }

    @Override
    public boolean loginUser(String login, String password) {
        for(User u : users) {
            if (u.getLogin().equals(login)) {
                if (u.getPassword().equals(password)) {
                    log.info("User [" + login + "] successfully logged in.");
                    return true;
                } else {
                    log.warn("Invalid password for user [" + login + "].");
                    return false;
                }
            }
        }
        log.warn("User login [" + login + "] not found!");
        return false;
    }

    @Override
    public boolean changeUserPassword(String login, String oldPassword, String newPassword) {
        for (User u : users) {
            if (u.getLogin().equals(login)) {
                if (u.getPassword().equals(oldPassword)) {
                    u.setPassword(newPassword);
                    log.info("Successfully changed password for user [" + login + "].");
                    return true;
                } else {
                    log.warn("Old password do not match!");
                    return false;
                }
            }
        }
        log.warn("User login [" + login + "] not found!");
        return false;
    }

    private boolean isAdult(User user) {

        if (Period.between(user.getDateOfBirth(), LocalDate.now()).getYears() >= 18)
            return true;
        else
            return false;
    }
}
