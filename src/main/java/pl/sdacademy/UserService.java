package pl.sdacademy;

public interface UserService {

    boolean registerUser(User user);

    boolean loginUser(String login, String password);

    boolean changeUserPassword(String login, String oldPassword, String newPassword);
}
