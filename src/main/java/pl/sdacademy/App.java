package pl.sdacademy;

import java.time.LocalDate;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        UserServiceImpl db = new UserServiceImpl();

        User usr1 = new User(1,"Kajetan","Zbyszko","zbychu","chujchuj","dupa@o2.pl", LocalDate.parse("2007-12-03"));
        User usr2 = new User(2,"Marian","Konował","koniu","dupacycki","pupa@o2.pl", LocalDate.parse("2001-05-03"));
        User usr3 = new User(3,"Pan","Koń","koniu","dupacycki","pooiupa@o2.pl", LocalDate.parse("1988-05-03"));


        db.registerUser(usr1);
        db.registerUser(usr2);
        db.registerUser(usr3);

        db.loginUser("koniu","dupacycki1");
        db.loginUser("koniu","dupacycki");

        db.changeUserPassword("koniu","dupa","newpass");
        db.changeUserPassword("koniu","dupacycki","newpass");
    }
}
